all : pcap_test

pcap_test: main.o get_address.o
	g++ -g -o pcap_test main.o get_address.o -lpcap

get_address.o:
	g++ -g -c -o get_address.o get_address.cpp

main.o:
	g++ -g -c -o main.o main.cpp

clean:
	rm -f pcap_test
	rm -f *.o

