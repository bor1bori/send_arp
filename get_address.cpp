/*
 * gethwaddr.c
 *
 * Demonstrates retrieving hardware address of adapter using ioctl()
 *
 * Author: Ben Menking <bmenking@highstream.net>
 *
 */
#include <stdio.h>
#include <unistd.h>
#include <string.h> /* for strncpy */
#include <sys/ioctl.h>
#include <sys/types.h>    
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "get_address.h"

int get_mac_addr(u_int8_t mac[6], char interface[]) {
    int fd;
    struct ifreq ifr;

    fd = socket(PF_INET, SOCK_DGRAM, 0);

    memset(&ifr, 0x00, sizeof(ifr));

    strcpy(ifr.ifr_name, interface);

    ioctl(fd, SIOCGIFHWADDR, &ifr);

    close(fd);

    memcpy(mac, ifr.ifr_hwaddr.sa_data, 6);

    return 0;
} 

int get_ip_addr(u_int8_t ip[4], char interface[]) {
    int fd;
    struct ifreq ifr;

    fd = socket(AF_INET, SOCK_DGRAM, 0);

    /* I want to get an IPv4 IP address */
    ifr.ifr_addr.sa_family = AF_INET;

    /* I want IP address attached to "interface" */
    strncpy(ifr.ifr_name, interface, IFNAMSIZ-1);

    ioctl(fd, SIOCGIFADDR, &ifr);

    close(fd);
    memcpy(ip, &((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr, 4);

    return 0;
}